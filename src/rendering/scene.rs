use std::sync::Arc;

use crate::color::{Color, Texture};
use crate::math::Transform;
use crate::rendering::Camera;
use crate::shapes::Object;

// TODO: Scene from glTF
/// The scene, consisting of camera, objects and background.
pub struct Scene {
    pub camera: Camera,
    pub objects: Vec<Object>,
    pub background_color: Color,
    pub background_texture: Option<Arc<dyn Texture>>,
    pub background_strength: f64,
    pub background_transform: Transform,
    pub tile_size_x: usize,
    pub tile_size_y: usize,
}

impl Default for Scene {
    fn default() -> Self {
        Scene {
            camera: Camera::default(),
            objects: vec![],
            background_color: Color::splat(0.8),
            background_texture: None,
            background_strength: 1.0,
            background_transform: Transform::default(),
            tile_size_x: 16,
            tile_size_y: 16,
        }
    }
}
