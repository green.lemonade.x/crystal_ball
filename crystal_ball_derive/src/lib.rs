mod derive_transformable;

extern crate proc_macro;

#[proc_macro_derive(Transformable, attributes(transform, internal))]
pub fn derive_transformable(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    derive_transformable::derive_transformable(input)
}
