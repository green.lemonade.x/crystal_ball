use crate::math::Point2;

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Bounds2 {
    pub min: Point2,
    pub max: Point2,
}

impl Default for Bounds2 {
    fn default() -> Self {
        Bounds2 {
            min: Point2::splat(f64::MAX),
            max: Point2::splat(f64::MIN),
        }
    }
}

impl Bounds2 {
    pub fn new(point_a: Point2, point_b: Point2) -> Self {
        Bounds2 {
            min: Point2::new(point_a.x.min(point_b.x), point_a.y.min(point_b.y)),
            max: Point2::new(point_a.x.max(point_b.x), point_a.y.max(point_b.y)),
        }
    }
}
