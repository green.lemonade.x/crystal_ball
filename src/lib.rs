#![cfg_attr(doc, feature(doc_cfg))]

//! # Overview
//!
//! *Crystal Ball* is a path tracing library written in Rust.
//!
//! It uses [rayon](https://github.com/rayon-rs/rayon) for parallelization
//! and can save the rendered image in various formats thanks to the [image](https://github.com/image-rs/image) crate.
//!
//! ## Example Usage
//! ```
//! use std::default::Default;
//! use std::sync::Arc;
//!
//! use crystal_ball::prelude::*;
//!
//! fn main() -> Result<(), Error> {
//! let objects = vec![
//!         Object::new(
//!             Arc::new(Sphere::new()),
//!             Arc::new(PbrMaterial {
//!                 base_color: Color::new(1.0, 0.45, 0.31),
//!                 ..Default::default()
//!             }),
//!         ),
//!         Object::new(
//!             Arc::new(
//!                 Sphere::new()
//!                     .translate(Vec3::new(0.0, -101.0, 0.0))
//!                     .scale_xyz(Vec3::splat(100.0)),
//!             ),
//!             Arc::new(PbrMaterial::default()),
//!         ),
//!     ];
//!
//!     let scene = Scene {
//!         objects,
//!         camera: Camera::default().translate(Vec3::new(0.0, 0.0, -5.0)),
//!         ..Default::default()
//!     };
//!
//!     let engine = RenderEngine::default();
//!     let image = engine.render(&scene);
//!
//!     image.write("basic.png")?;
//!
//!     Ok(())
//! }
//! ```

pub use image::ImageError;
#[cfg(feature = "oidn")]
pub use oidn::FilterError;

pub mod color;
pub mod materials;
pub mod math;
pub mod prelude;
pub mod rendering;
pub mod shapes;
pub mod util;
