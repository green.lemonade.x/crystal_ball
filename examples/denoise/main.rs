use std::sync::Arc;

use crystal_ball::prelude::*;

fn main() -> Result<(), Error> {
    let objects = vec![
        Object::new(
            Arc::new(
                Sphere::new()
                    .translate(Vec3::new(1.0, 0.0, 0.0))
                    .scale_xyz(Vec3::splat(0.5)),
            ),
            Arc::new(PbrMaterial {
                base_color: Color::new(0.8, 0.6, 0.2),
                metallic: 1.0,
                ..Default::default()
            }),
        ),
        Object::new(
            Arc::new(Sphere::new().scale_xyz(Vec3::splat(0.5))),
            Arc::new(PbrMaterial {
                base_color: Color::new(1.0, 0.45, 0.31),
                ..Default::default()
            }),
        ),
        Object::new(
            Arc::new(
                Sphere::new()
                    .translate(Vec3::new(0.1, 1.0, 0.0))
                    .scale_xyz(Vec3::splat(0.1)),
            ),
            Arc::new(PbrMaterial {
                base_color: Color::BLACK,
                emissive_color: Color::splat(1.0),
                emissive_strength: 20.0,
                ..Default::default()
            }),
        ),
        Object::new(
            Arc::new(
                Sphere::new()
                    .translate(Vec3::new(-1.0, 0.0, 0.0))
                    .scale_xyz(Vec3::splat(0.5)),
            ),
            Arc::new(PbrMaterial {
                base_color: Color::splat(0.9),
                metallic: 1.0,
                roughness: 0.3,
                ..Default::default()
            }),
        ),
        Object::new(
            Arc::new(
                Sphere::new()
                    .translate(Vec3::new(0.0, -100.5, 0.0))
                    .scale_xyz(Vec3::splat(100.0)),
            ),
            Arc::new(PbrMaterial {
                base_color: Color::splat(0.7),
                ..Default::default()
            }),
        ),
    ];

    let position = Point3::new(3.0, 2.0, 1.0);
    let target = Point3::ZERO;

    let scene = Scene {
        camera: Camera {
            lens_radius: 0.2,
            focal_distance: (target - position).magnitude(),
            ..Default::default()
        }
        .translate(position.to_vec3())
        .look_at(target, Vec3::Y),
        objects,
        ..Default::default()
    };

    let engine = RenderEngine {
        max_bounces: 8,
        ..Default::default()
    };
    let mut image = engine.render(&scene);

    image.write("image.png")?;

    image.denoise()?;
    image.write("image_denoised.png")?;

    Ok(())
}
