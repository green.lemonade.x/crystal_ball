//! Data Types and functions related to colors and images.

pub use color::Color;

pub use self::image::{ColorFormat, Image, Interpolation};
// Use `self` to avoid name conflict with the `image` crate.
pub(crate) use self::image::ImageTile;

mod color;
mod image;

pub trait Texture: Send + Sync {
    fn get_pixel(&self, u: f64, v: f64) -> Color;
}
