use crystal_ball::prelude::*;

fn main() -> Result<(), Error> {
    let scene = Scene {
        camera: Camera::default()
            .translate(Vec3::new(1.5, 1.0, 3.0))
            .look_at(Point3::ZERO, Vec3::Y),
        objects: Object::load_gltf("examples/gltf/book.gltf")?,
        ..Default::default()
    };

    let engine = RenderEngine {
        samples: 128,
        max_bounces: 8,
    };
    let image = engine.render(&scene);

    image.write("gltf.png")?;

    Ok(())
}
