use std::sync::{Arc, Mutex};
use std::time::Instant;

use indicatif::{ProgressBar, ProgressStyle};
use nanorand::tls::TlsWyRand;
use nanorand::tls_rng;
use rayon::prelude::*;

use crate::color::{Color, Image, ImageTile, Interpolation};
use crate::math::{Hit, Ray};
use crate::rendering::{PrecalculatedCamera, Scene};
use crate::rendering::camera::CameraSample;
use crate::shapes::{BVH, Object, Sphere};
use crate::util::compute_color;

/// The render engine.
#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
pub struct RenderEngine {
    pub samples: usize,
    pub max_bounces: usize,
}

impl Default for RenderEngine {
    fn default() -> Self {
        Self {
            samples: 64,
            max_bounces: 4,
        }
    }
}

impl RenderEngine {
    /// Return the closest hit of a ray with objects of the scene, if any.
    // TODO: Make the render_engine own the scene (also simplifies lifetimes)
    fn get_closest_hit<'a>(&self, bvh: &'a BVH<Object>, ray: Ray) -> Option<(Hit, &'a Object)> {
        bvh.intersects(ray)
    }

    /// Trace a ray and return the resulting color.
    fn trace_tray(
        &self,
        bvh: &BVH<Object>,
        scene: &Scene,
        ray: Ray,
        bounces: usize,
        rng: &mut TlsWyRand,
    ) -> Color {
        if bounces > self.max_bounces {
            return Color::default();
        }

        let closest_hit = self.get_closest_hit(bvh, ray);

        match closest_hit {
            None => {
                let (u, v) = Sphere::new().uv_map(
                    (scene.background_transform.mat4 * ray.direction)
                        .normalize()
                        .into(),
                );

                compute_color(
                    scene.background_color,
                    &scene.background_texture,
                    scene.background_strength,
                    u,
                    v,
                )
            }
            Some((hit, object)) => {
                let outgoing = object.material.next_ray(ray, hit, rng);

                let traced = match outgoing {
                    None => Color::default(),
                    Some(ray) => self.trace_tray(bvh, scene, ray, bounces + 1, rng),
                };

                object
                    .material
                    .compute_color(ray, outgoing, hit, traced)
            }
        }
    }

    /// Render the give scene.
    pub fn render(&self, scene: &Scene) -> Image {
        let camera: PrecalculatedCamera = scene.camera.into();

        let (width, height) = camera.dimensions();

        println!(
            "Rendering {} object(s) with {} samples ({} bounces) to a {}x{} image.",
            scene.objects.len(),
            self.samples,
            self.max_bounces,
            width,
            height
        );

        let bvh = BVH::init(4, scene.objects.clone());

        let tile_size_x = scene.tile_size_x.min(width as usize - 1);
        let tile_size_y = scene.tile_size_y.min(height as usize - 1);

        let mut tiles = vec![];
        for x in 0..(width as f64 / tile_size_x as f64).ceil() as usize {
            for y in 0..(height as f64 / tile_size_y as f64).ceil() as usize {
                tiles.push(ImageTile::new(
                    x,
                    y,
                    Image::new(tile_size_x, tile_size_y, Interpolation::Closest),
                ))
            }
        }

        let progress_bar = ProgressBar::new((tiles.len()) as u64);
        progress_bar.set_style(
            ProgressStyle::default_bar()
                .template(
                    "[{elapsed_precise}] [{bar:60}] {percent:>5}% (ETA: ~{eta_precise})\n{msg}",
                )
                .progress_chars("=> "),
        );
        progress_bar.set_position(0);
        let progress_bar = Arc::new(Mutex::new(progress_bar));

        let samples_inverse = 1.0 / self.samples as f64;

        let start_time = Instant::now();

        tiles.par_iter_mut().for_each(|tile| {
            let tile_x = tile.x();
            let tile_y = tile.y();

            tile.image_mut()
                .pixels
                .iter_mut()
                .enumerate()
                .for_each(|(y, row)| {
                    row.iter_mut().enumerate().for_each(|(x, pixel)| {
                        let x = x + scene.tile_size_x * tile_x;
                        let y = y + scene.tile_size_y * tile_y;

                        let mut rng = tls_rng();
                        let mut color = Color::default();

                        for _ in 0..self.samples {
                            let sample =
                                CameraSample::new(x as f64, y as f64, &mut rng, height as f64);

                            let ray = camera.get_ray(sample);

                            color += self.trace_tray(&bvh, scene, ray, 0, &mut rng);
                        }

                        *pixel = (color * samples_inverse).linear_to_srgb();
                        *pixel = Color::new(
                            pixel.r.clamp(0.0, 1.0),
                            pixel.g.clamp(0.0, 1.0),
                            pixel.b.clamp(0.0, 1.0),
                        );
                    });
                });

            progress_bar.lock().unwrap().inc(1);
        });

        progress_bar.lock().unwrap().finish_with_message(format!(
            "Rendering finished after {:.2?}",
            Instant::now() - start_time
        ));

        Image::from_tiles(
            width as usize,
            height as usize,
            &tiles,
            Interpolation::Closest,
        )
    }
}
