use crystal_ball::math::Transform;
use crystal_ball_derive::Transformable;

#[derive(Transformable)]
pub struct Shape {
    #[transform]
    transform: Transform,
}

fn main() {}
