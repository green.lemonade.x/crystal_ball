# Basic

A basic example.

Use `cargo run --example basic` to run.

## Output

![Basic](render.png)