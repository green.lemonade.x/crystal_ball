use std::default::Default;
use std::sync::Arc;

use crystal_ball::prelude::*;

fn main() -> Result<(), Error> {
    let objects = vec![
        Object::new(
            Arc::new(Sphere::new()),
            Arc::new(PbrMaterial {
                base_color: Color::BLACK,
                // Image texture by Solar System Scope (https://www.solarsystemscope.com/textures/download/2k_earth_daymap.jpg).
                emissive_texture: Some(Arc::new(Image::from_file(
                    "examples/image_texture/earth.jpg",
                    Interpolation::Bilinear,
                )?)),
                emissive_strength: 1.0,
                ..Default::default()
            }),
        ),
        Object::new(
            Arc::new(
                Sphere::new()
                    .translate(Vec3::new(-2.5, 1.0, 2.0))
                    .scale_xyz(Vec3::splat(0.1)),
            ),
            Arc::new(PbrMaterial {
                base_color: Color::BLACK,
                // Image texture by Solar System Scope (https://www.solarsystemscope.com/textures/download/2k_moon.jpg).
                emissive_texture: Some(Arc::new(Image::from_file(
                    "examples/image_texture/moon.jpg",
                    Interpolation::Bilinear,
                )?)),
                emissive_strength: 1.0,
                ..Default::default()
            }),
        ),
        Object::new(
            Arc::new(
                Sphere::new()
                    .translate(Vec3::new(0.0, -101.0, 0.0))
                    .scale_xyz(Vec3::splat(100.0)),
            ),
            Arc::new(PbrMaterial {
                base_color: Color::WHITE,
                metallic: 1.0,
                roughness: 0.3,
                ..Default::default()
            }),
        ),
    ];

    let scene = Scene {
        camera: Camera::default()
            .translate(Vec3::new(-5.0, 1.0, 2.0))
            .look_at(Point3::ZERO, Vec3::Y),
        background_color: Color::splat(0.0),
        objects,
        ..Default::default()
    };

    let engine = RenderEngine::default();
    let image = engine.render(&scene);

    image.write("image_texture.png")?;

    Ok(())
}
