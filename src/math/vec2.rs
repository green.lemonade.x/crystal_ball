use std::convert::TryFrom;
use std::f64::consts::{FRAC_PI_2, FRAC_PI_4};
use std::ops::{
    Add, AddAssign, Div, DivAssign, Index, IndexMut, Mul, MulAssign, Neg, Sub, SubAssign,
};

use nanorand::tls::TlsWyRand;

use crate::math::Point2;
use crate::util::random_float;

/// A 3D vector.
/// This struct supports elementwise arithmetic operations (+, -, *, /).
/// In addition [`dot`](Vec2::dot_product) and [`cross`](Vec2::cross_product) are provided.
#[derive(Copy, Clone, Default, Debug, PartialEq)]
pub struct Vec2 {
    pub x: f64,
    pub y: f64,
}

impl Vec2 {
    pub const X: Vec2 = Vec2 { x: 1.0, y: 0.0 };

    pub const Y: Vec2 = Vec2 { x: 0.0, y: 1.0 };

    pub const ZERO: Vec2 = Vec2 { x: 0.0, y: 0.0 };

    /// Create a new 3D vector.
    pub fn new(x: f64, y: f64) -> Self {
        Vec2 { x, y }
    }

    pub fn splat(value: f64) -> Self {
        Vec2 { x: value, y: value }
    }

    /// Generate a Vec2 where each component is a uniform random number between `min` and `max`.
    pub fn random(rng: &mut TlsWyRand, min: f64, max: f64) -> Self {
        Vec2 {
            x: random_float(rng, min, max),
            y: random_float(rng, min, max),
        }
    }

    /// Generate a random Vec2 on the surface of the unit sphere with uniform (i.e. isotropic) distribution.
    pub fn random_unit_vector(rng: &mut TlsWyRand) -> Self {
        Self::random_in_unit_disk(rng).normalize()
    }

    pub fn sample_concentric_on_disc(&self) -> Self {
        let offset: Vec2 = 2.0 * *self - Vec2::new(1.0, 1.0);
        if offset.x == 0.0 && offset.y == 0.0 {
            return Vec2::new(0.0, 0.0);
        }
        let (theta, r) = if offset.x.abs() > offset.y.abs() {
            (FRAC_PI_4 * offset.y / offset.x, offset.x)
        } else {
            (FRAC_PI_2 - FRAC_PI_4 * offset.x / offset.y, offset.y)
        };
        r * Vec2::new(theta.cos(), theta.sin())
    }

    /// Generate a random Vec2 inside the unit disk with uniform distribution.
    pub fn random_in_unit_disk(rng: &mut TlsWyRand) -> Self {
        Vec2::random(rng, 0.0, 1.0).sample_concentric_on_disc()
    }

    /// Reflect the Vec2 with respect to the given normal.
    pub fn reflect(self, normal: Vec2) -> Self {
        self - 2.0 * Self::dot(self, normal) * normal
    }

    pub fn to_point2(&self) -> Point2 {
        (*self).into()
    }

    /// Return the magnitude (length).
    pub fn magnitude(&self) -> f64 {
        Self::dot(*self, *self).sqrt()
    }

    pub fn magnitude_squared(&self) -> f64 {
        Self::dot(*self, *self)
    }

    /// Return the absolute value (synonym [`magnitude`](Self::magnitude)).
    pub fn abs(&self) -> f64 {
        self.magnitude()
    }

    /// Return the unit vector parallel to self.
    /// Panics if self cannot be normalized.
    pub fn normalize(&self) -> Self {
        assert_ne!(self.magnitude(), 0.0, "Can't normalize zero vector");

        *self / self.magnitude()
    }

    /// Calculate the dot product.
    pub fn dot(vec_a: Vec2, vec_b: Vec2) -> f64 {
        vec_a.x * vec_b.x + vec_a.y * vec_b.y
    }
}

impl Add<Vec2> for Vec2 {
    type Output = Vec2;

    fn add(self, rhs: Vec2) -> Self::Output {
        Vec2 {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl AddAssign<Vec2> for Vec2 {
    fn add_assign(&mut self, rhs: Vec2) {
        *self = *self + rhs;
    }
}

impl Sub<Vec2> for Vec2 {
    type Output = Vec2;

    fn sub(self, rhs: Vec2) -> Self::Output {
        Vec2 {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}

impl SubAssign<Vec2> for Vec2 {
    fn sub_assign(&mut self, rhs: Vec2) {
        *self = *self - rhs;
    }
}

impl Mul<f64> for Vec2 {
    type Output = Vec2;

    fn mul(self, rhs: f64) -> Self::Output {
        Vec2 {
            x: self.x * rhs,
            y: self.y * rhs,
        }
    }
}

impl MulAssign<f64> for Vec2 {
    fn mul_assign(&mut self, rhs: f64) {
        *self = *self * rhs;
    }
}

impl Mul<Vec2> for f64 {
    type Output = Vec2;

    fn mul(self, rhs: Vec2) -> Self::Output {
        rhs * self
    }
}

impl Div<f64> for Vec2 {
    type Output = Vec2;

    fn div(self, rhs: f64) -> Self::Output {
        let rhs_inverse = 1.0 / rhs;

        Vec2 {
            x: self.x * rhs_inverse,
            y: self.y * rhs_inverse,
        }
    }
}

impl DivAssign<f64> for Vec2 {
    fn div_assign(&mut self, rhs: f64) {
        *self = *self / rhs;
    }
}

impl Neg for Vec2 {
    type Output = Vec2;

    fn neg(self) -> Self::Output {
        Vec2 {
            x: -self.x,
            y: -self.y,
        }
    }
}

impl From<[f64; 2]> for Vec2 {
    fn from(s: [f64; 2]) -> Self {
        Vec2 { x: s[0], y: s[1] }
    }
}

impl From<(f64, f64)> for Vec2 {
    fn from(t: (f64, f64)) -> Self {
        Vec2 { x: t.0, y: t.1 }
    }
}

impl From<Point2> for Vec2 {
    fn from(p: Point2) -> Self {
        Vec2 { x: p.x, y: p.y }
    }
}

impl TryFrom<Vec<f64>> for Vec2 {
    type Error = &'static str;

    fn try_from(v: Vec<f64>) -> Result<Self, Self::Error> {
        if v.len() != 2 {
            Err("Vec2 can only be build from a vector of length 3.")
        } else {
            Ok(Vec2 { x: v[0], y: v[1] })
        }
    }
}

impl TryFrom<&[f64]> for Vec2 {
    type Error = &'static str;

    fn try_from(s: &[f64]) -> Result<Self, Self::Error> {
        if s.len() != 2 {
            Err("Vec2 can only be build from a slice of length 3.")
        } else {
            Ok(Vec2 { x: s[0], y: s[1] })
        }
    }
}

impl Index<usize> for Vec2 {
    type Output = f64;

    fn index(&self, index: usize) -> &Self::Output {
        match index {
            0 => &self.x,
            1 => &self.y,
            _ => panic!(
                "index out of bounds: the len is 2 but the index is {}",
                index
            ),
        }
    }
}

impl IndexMut<usize> for Vec2 {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        match index {
            0 => &mut self.x,
            1 => &mut self.y,
            _ => panic!(
                "index out of bounds: the len is 2 but the index is {}",
                index
            ),
        }
    }
}
