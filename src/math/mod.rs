//! Data Types and functions related to math with focus on vectors.
pub use bounds2::Bounds2;
pub use bounds3::Bounds3;
pub use hit::Hit;
pub use mat4::Mat4;
pub use point2::Point2;
pub use point3::Point3;
pub use ray::Ray;
pub use ray::RayType;
pub use transform::{Transform, Transformable};
pub use vec2::Vec2;
pub use vec3::Vec3;

#[derive(Copy, Clone, Debug)]
pub enum XYZEnum {
    X = 0,
    Y = 1,
    Z = 2,
}

impl Default for XYZEnum {
    fn default() -> Self {
        Self::X
    }
}

mod bounds2;
mod bounds3;
mod hit;
mod mat4;
mod point2;
mod point3;
mod ray;
mod transform;
mod vec2;
mod vec3;
