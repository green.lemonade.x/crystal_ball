# Changelog

## Version 0.2

- Faster render times by using a [faster random number generation library](https://github.com/aspenluxxxy/nanorand-rs)
- Use tiled rendering instead of row-based
- Spheres can now also have textures
- Change license from GPLv3 to LGPLv3