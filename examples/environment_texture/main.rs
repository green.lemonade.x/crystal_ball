use std::default::Default;
use std::f64::consts::PI;
use std::sync::Arc;

use crystal_ball::prelude::*;

fn main() -> Result<(), Error> {
    let scene = Scene {
        background_color: Color::WHITE,
        background_texture: Some(Arc::new(
            // Environment texture by HDRI Haven (https://hdrihaven.com/hdri/?h=spruit_sunrise).
            // If you don't have an HDRI use `Image::from_file()`.
            Image::from_hdr_file(
                "examples/environment_texture/spruit_sunrise_1k.hdr",
                Interpolation::Bilinear,
            )?,
        )),
        background_transform: Transform::default().rotate_y(PI),
        objects: vec![Object::new(
            Arc::new(Sphere::new()),
            Arc::new(PbrMaterial {
                base_color: Color::WHITE,
                metallic: 1.0,
                ..Default::default()
            }),
        )],
        camera: Camera::default().translate(Vec3::new(0.0, 0.0, 5.0)),
        ..Default::default()
    };

    let render_engine = RenderEngine {
        max_bounces: 1,
        ..Default::default()
    };
    let image = render_engine.render(&scene);

    image.write("environment_texture.png")?;

    Ok(())
}
