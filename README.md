# crystal_ball

[![Latest Version](https://img.shields.io/crates/v/crystal_ball.svg)](https://crates.io/crates/crystal_ball)
[![API Documentation](https://docs.rs/crystal_ball/badge.svg)](https://docs.rs/crystal_ball)

*Crystal Ball* is a path tracing library written in Rust. It uses [rayon](https://github.com/rayon-rs/rayon) for
parallelization and can save the rendered image in various formats thanks to
the [image](https://github.com/image-rs/image) crate.

![](demo.png)

## Features

- Multithreaded CPU rendering
- Save rendered images in [various formats](https://github.com/image-rs/image#supported-image-formats)
- Environment textures
- Materials: diffuse, metallic, emissive, refractive
- Shapes: currently only spheres are supported
- Create your own textures, materials, and shapes
- Depth of field
- Optional denoising using [OpenImageDenoise](https://www.openimagedenoise.org/)

## Usage

### Getting Started

Create a new project:

```
cargo init [project-name]
cd [project-name]
```

Add `crystal_ball = 0.2.0` to the `[dependencies]` section of the `Cargo.toml` file.

Replace the content of `src/main.rs` with the following minimal example:

```rust
use crystal_ball::prelude::*;

fn main() -> Result<(), Error> {
    let objects = vec![
        Object::new(Sphere::new(), Diffuse::new(Color::new(1.0, 0.45, 0.31))),
        Object::new(
            Sphere::new()
                .translate(Vec3::new(0.0, -101.0, 0.0))
                .scale_xyz(Vec3::splat(100.0)),
            Diffuse::new(Color::splat(0.8)),
        ),
    ];

    let scene = Scene::new().with_objects(objects);

    let engine = RenderEngine::new(64, 4); // Samples and max bounces
    let image = engine.render(&scene);

    image
        .write("example_render.png")?;
}
```

Compile and run the project using `cargo run --release`.

### Denoising

Crystal ball can optionally denoise rendered images using the [Rust bindings](https://github.com/Twinklebear/oidn-rs)
for [OpenImageDenoise](https://www.openimagedenoise.org/).

To use this feature you need to [install OpenImageDenoise](https://www.openimagedenoise.org/downloads.html)
and set the environment variable `OIDN_DIR` to the root directory of the OpenImageDenoise installation.

Enable the `oidn` feature in your `Cargo.toml`:

```toml
[dependencies.crystal_ball]
version = "0.2.0"
features = ["oidn"]
```

Now you can use the denoise method in your Rust code.

```rust
fn main() {
    // ...
    let mut image = engine.render(&scene);
    image
        .denoise()
        .write("denoised_render.png")
        .expect("Error denoising image")?;
}
```

## Optimization

The performance of your path tracing code will benefit greatly from compiler optimizations, at the cost of longer
compile times. To easily switch between quick compilation and highly optimized code generation, you can put the
following lines into your `Cargo.toml`.

```toml
# TODO: no longer requires nightly
# Required by `strip = "symbols"` (requires nightly toolchain)
#cargo-features = ["strip"]

[package]
# ...
[profile.dev]
opt-level = 3

[profile.release]
# Reduce binary size
panic = "abort"
#strip = "symbols"
# Improve performance but increase compile time
lto = "fat"
codegen-units = 1
```

With this in place you can quickly compile using `cargo run`. To get full optimization use `cargo run --release`.

To massively reduce the size of the executable, you can use `strip = symbols`, provided you have the `strip` cargo
feature (`cargo-features = ["strip"]`). However, this requires the nightly toolchain.

## References/Resources

- https://raytracing.github.io/books/RayTracingInOneWeekend.html
- https://github.com/ekzhang/rpt
- https://tavianator.com/2014/ray_triangle.html
- https://www.scratchapixel.com/lessons/3d-basic-rendering/ray-tracing-rendering-a-triangle/moller-trumbore-ray-triangle-intersection
- https://en.wikipedia.org/wiki/Möller–Trumbore_intersection_algorithm