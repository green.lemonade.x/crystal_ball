use crate::math::{Point3, Vec3};

/// A ray (half-line).
#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Ray {
    pub origin: Point3,
    pub direction: Vec3,
    pub ray_type: RayType,
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum RayType {
    Metallic,
    Dielectric,
    Diffuse,
}

impl Ray {
    /// Create a new ray.
    pub fn new(origin: Point3, direction: Vec3, ray_type: RayType) -> Self {
        let direction = direction.normalize();
        Ray {
            // Shift the intersection point slightly outwards in order to avoid problems due to finite floating point precision.
            origin: origin + direction * 1e-9,
            direction,
            ray_type,
        }
    }

    /// Return the point on the ray at the given distance.
    pub fn get(&self, distance: f64) -> Point3 {
        self.origin + self.direction * distance
    }
}

#[cfg(test)]
mod tests {
    use assert_approx_eq::assert_approx_eq;

    use crate::math::{Point3, RayType};
    use crate::util::EPSILON;

    use super::{Ray, Vec3};

    #[test]
    fn ray_get_point() {
        assert_approx_eq!(
            Ray::new(
                Point3::new(0.0, 0.0, 0.0),
                Vec3::new(1.0, 0.0, 0.0),
                RayType::Dielectric
            )
            .get(2.0),
            Point3::new(2.0, 0.0, 0.0),
            EPSILON + 1e-9
        );
        assert_approx_eq!(
            Ray::new(
                Point3::new(5.72, 2.5, 8.824),
                Vec3::new(8.7, 5.987, 0.12),
                RayType::Dielectric
            )
            .get(2.5),
            Point3::new(7.779336228, 3.917154712, 8.852404638),
            EPSILON + 1e-9
        );
    }
}
