use std::f32::consts::TAU;
use std::f64::consts::PI;
use std::sync::Arc;

use nanorand::tls::TlsWyRand;

use crate::color::{Color, Texture};
use crate::materials::Material;
use crate::math::{Hit, Point2, Ray, RayType, Vec2, Vec3};
use crate::prelude::IOR;
use crate::util::random_float;

// TODO: metallic, roughness and transmission texture
// A combined materials for general purpose.
#[derive(Clone)]
pub struct PbrMaterial {
    pub base_color: Color,
    pub base_color_texture: Option<Arc<dyn Texture>>,
    pub metallic: f64,
    pub roughness: f64,
    pub specular: f64,
    pub transmission: f64,
    pub ior: f64,
    // RGB IOR values at wavelengths 650nm (red), 550nm (green), 450nm (Blue)
    pub rgb_ior_n: Color,
    pub rgb_ior_k: Color,
    pub emissive_color: Color,
    pub emissive_texture: Option<Arc<dyn Texture>>,
    pub emissive_strength: f64,
}

impl Default for PbrMaterial {
    fn default() -> Self {
        Self {
            base_color: Color::splat(0.8),
            base_color_texture: None,
            metallic: 0.0,
            specular: 0.0,
            roughness: 0.0,
            transmission: 0.0,
            ior: IOR::GLASS,
            // Values for iron
            rgb_ior_n: Color::new(2.91140, 2.94970, 2.58450),
            rgb_ior_k: Color::new(3.08930, 2.93180, 2.76700),
            emissive_color: Color::WHITE,
            emissive_texture: None,
            emissive_strength: 0.0,
        }
    }
}

impl PbrMaterial {
    fn roughness_to_alpha(&self) -> f64 {
        self.roughness.powi(2)
    }

    // Implementation according to https://www.pbr-book.org/3ed-2018/Reflection_Models/Microfacet_Models
    fn trowbridge_reitz_d(&self, wh: Vec3) -> f64 {
        let cos2_theta = wh.z.powi(2);
        let sin2_theta = (1.0 - cos2_theta).max(0.0);
        let tan2_theta = sin2_theta / cos2_theta;

        if tan2_theta.is_infinite() {
            return 0.0;
        }
        let cos4_theta = cos2_theta.powi(2);

        let sin_theta = sin2_theta.sqrt();
        let sin2_phi = if sin_theta == 0.0 {
            1.0
        } else {
            (wh.y / sin_theta).clamp(-1.0, 1.0).powi(2)
        };
        let cos2_phi = if sin_theta == 0.0 {
            1.0
        } else {
            (wh.x / sin_theta).clamp(-1.0, 1.0).powi(2)
        };

        let alpha = self.roughness_to_alpha();

        // Assuming alphax = alphay = alpha
        let e = (cos2_phi + sin2_phi) / alpha.powi(2) * tan2_theta;
        1.0 / (PI * alpha.powi(2) * cos4_theta * (1.0 + e.powi(2)))
    }

    fn trowbridge_reitz_lambda(&self, w: Vec3) -> f64 {
        let cos_theta = w.z;
        let cos2_theta = cos_theta.powi(2);
        let sin2_theta = (1.0 - cos2_theta).max(0.0);
        let sin_theta = sin2_theta.sqrt();
        let abs_tan_theta = (sin_theta / cos_theta).abs();

        if abs_tan_theta.is_infinite() {
            return 0.0;
        }
        let sin2_phi = if sin_theta == 0.0 {
            1.0
        } else {
            (w.y / sin_theta).clamp(-1.0, 1.0).powi(2)
        };
        let cos2_phi = if sin_theta == 0.0 {
            1.0
        } else {
            (w.x / sin_theta).clamp(-1.0, 1.0).powi(2)
        };

        let alpha = self.roughness_to_alpha();
        let alpha = (cos2_phi * alpha.powi(2) + sin2_phi * alpha.powi(2)).sqrt();

        let alpha2_tan2_theta = (alpha * abs_tan_theta).powi(2);

        (-1.0 + (1.0 + alpha2_tan2_theta).sqrt()) / 2.0
    }

    fn trowbridge_reitz_g(&self, wo: Vec3, wi: Vec3) -> f64 {
        1.0 / (1.0 + self.trowbridge_reitz_lambda(wo) + self.trowbridge_reitz_lambda(wi))
    }
    fn trowbridge_reitz_g1(&self, w: Vec3) -> f64 {
        1.0 / (1.0 + self.trowbridge_reitz_lambda(w))
    }

    fn trowbridge_reitz_pdf(&self, wo: Vec3, wh: Vec3) -> f64 {
        self.trowbridge_reitz_d(wh) * self.trowbridge_reitz_g1(wo) * Vec3::dot(wo, wh).abs()
            / wh.z.abs()
    }

    fn trowbridge_reitz_sample_wh(&self, wo: Vec3, u: Point2) -> Vec3 {
        let alpha = self.roughness_to_alpha();
        if wo.z.is_sign_negative() {
            let wh = Self::trowbridge_reitz_sample(-wo, alpha, alpha, u);
            -wh
        } else {
            Self::trowbridge_reitz_sample(wo, alpha, alpha, u)
        }
    }
    fn trowbridge_reitz_sample(wi: Vec3, alpha_x: f64, alpha_y: f64, u: Point2) -> Vec3 {
        // 1. stretch wi
        let wi_stretched = Vec3 {
            x: alpha_x * wi.x,
            y: alpha_y * wi.y,
            z: wi.z,
        }
        .normalize();

        // 2. simulate P22_{wi}(x_slope, y_slope, 1, 1)
        let mut slope = Self::trowbridge_reitz_sample_11(wi_stretched.z, u.x, u.y);

        let cos_theta = wi_stretched.z;
        let cos2_theta = cos_theta.powi(2);
        let sin2_theta = (1.0 - cos2_theta).max(0.0);
        let sin_theta = sin2_theta.sqrt();

        let sin_phi = if sin_theta == 0.0 {
            1.0
        } else {
            (wi_stretched.y / sin_theta).clamp(-1.0, 1.0)
        };
        let cos_phi = if sin_theta == 0.0 {
            1.0
        } else {
            (wi_stretched.x / sin_theta).clamp(-1.0, 1.0)
        };

        // 3. rotate
        let tmp: f64 = cos_phi * slope.x - sin_phi * slope.y;
        slope.y = sin_phi * slope.x + cos_phi * slope.y;
        slope.x = tmp;

        // 4. unstretch
        slope.x *= alpha_x;
        slope.y *= alpha_y;

        // 5. compute normal
        Vec3 {
            x: -slope.x,
            y: -slope.y,
            z: 1.0,
        }
        .normalize()
    }
    fn trowbridge_reitz_sample_11(cos_theta: f64, u1: f64, u2: f64) -> Vec2 {
        let mut slope_x;
        let mut slope_y;

        // special case (normal incidence)
        if cos_theta > 0.9999 {
            let r = (u1 / (1.0 - u1)).sqrt();
            let phi = TAU * u2;
            return Vec2::new(r * phi.cos(), r * phi.sin());
        }

        let sin_theta = 0.0.max(1.0 - cos_theta.powi(2)).sqrt();
        let tan_theta = sin_theta / cos_theta;
        let b = tan_theta;

        let a = 1.0 / tan_theta;
        let g1 = 2.0 / (1.0 + (1.0 + b.powi(2)).sqrt());

        // sample slope_x
        let a = 2.0 * u1 / g1 - 1.0;
        let mut tmp = (1.0 / (a.powi(2) - 1.0)).min(1e10);

        let d = ((b * tmp).powi(2) - (a + b) * (a - b) * tmp)
            .max(0.0)
            .sqrt();
        let slope_x_1 = b * tmp - d;
        let slope_x_2 = b * tmp + d;
        if a < 0.0 || slope_x_2 > 1.0 / tan_theta {
            slope_x = slope_x_1;
        } else {
            slope_x = slope_x_2;
        }

        // sample slope_y
        let s;
        let new_u2 = if u2 > 0.5 {
            s = 1.0;
            2.0 * (u2 - 0.5)
        } else {
            s = -1.0;
            2.0 * (0.5 - u2)
        };
        let z = (new_u2 * (new_u2 * (new_u2 * 0.27385 - 0.73369) + 0.46341))
            / (new_u2 * (new_u2 * (new_u2 * 0.093_073 + 0.309_420) - 1.0) + 0.597_999);
        slope_y = s * z * (1.0 + slope_x.powi(2)).sqrt();

        assert!(slope_y.is_finite());
        assert!(!slope_y.is_nan());
        Vec2::new(slope_x, slope_y)
    }

    fn trowbridge_reitz_sample_f(
        &self,
        wo: Vec3,
        wi: &mut Vec3,
        u: Point2,
        pdf: &mut f64,
        _sampled_type: &mut u8,
    ) -> Color {
        // sample microfacet orientation $\wh$ and reflected direction $\wi$
        if wo.z == 0.0 {
            return Color::default();
        }
        let wh: Vec3 = self.trowbridge_reitz_sample_wh(wo, u);
        *wi = Vec3::reflect(wo, wh);
        if (wo.z * wi.z).is_sign_negative() {
            return Color::default();
        }
        // compute PDF of _wi_ for microfacet reflection
        *pdf = self.trowbridge_reitz_pdf(wo, wh) / (4.0 * Vec3::dot(wo, wh));
        self.get_base_color(u) * self.f(wo, &*wi)
    }

    fn get_base_color(&self, uv: Point2) -> Color {
        match self.base_color_texture.clone() {
            None => self.base_color,
            Some(texture) => self.base_color * texture.get_pixel(uv.x, uv.y),
        }
    }

    fn get_emissive_color(&self, uv: Point2) -> Color {
        match self.emissive_texture.clone() {
            None => self.emissive_color * self.emissive_strength,
            Some(texture) => {
                self.emissive_color * texture.get_pixel(uv.x, uv.y) * self.emissive_strength
            }
        }
    }

    pub fn dielectric_bsdf(&self, ray: Ray, hit: Hit, rng: &mut TlsWyRand) -> Option<Ray> {
        let cos_theta_i = Vec3::dot(-ray.direction, hit.normal);
        let fresnel = Self::fresnel_dielectric(cos_theta_i, IOR::AIR, self.ior);

        let rand = random_float(rng, 0.0, 1.0);

        if rand > fresnel {
            if random_float(rng, 0.0, 1.0) > self.transmission {
                return self.diffuse_brdf(hit, rng);
            }

            let (ior1, ior2) = if cos_theta_i < 0.0 {
                (self.ior, IOR::AIR)
            } else {
                (IOR::AIR, self.ior)
            };

            let direction = ray.direction.refract(hit.normal, ior1, ior2).normalize();
            Some(Ray::new(
                hit.position,
                direction + self.roughness * Vec3::random_in_unit_sphere(rng),
                RayType::Dielectric,
            ))
        } else {
            let direction = ray.direction.reflect(hit.normal);
            Some(Ray::new(
                hit.position,
                direction + self.roughness * Vec3::random_in_unit_sphere(rng),
                RayType::Dielectric,
            ))
        }
    }

    fn fresnel_dielectric(mut cos_theta_i: f64, mut eta_i: f64, mut eta_t: f64) -> f64 {
        cos_theta_i = cos_theta_i.clamp(-1.0, 1.0);

        if cos_theta_i < 0.0 {
            (eta_i, eta_t) = (eta_t, eta_i);
            cos_theta_i = cos_theta_i.abs();
        }

        let sin_theta_i = 0.0_f64.max(1.0 - cos_theta_i.powi(2)).sqrt();
        let sin_theta_t = eta_i / eta_t * sin_theta_i;

        if sin_theta_t > 1.0 {
            return 1.0;
        }

        let cos_theta_t = 0.0_f64.max(1.0 - sin_theta_t.powi(2)).sqrt();

        let r_parl = (eta_t * cos_theta_i - eta_i * cos_theta_t)
            / (eta_t * cos_theta_i + eta_i * cos_theta_t);
        let r_perp = (eta_i * cos_theta_i - eta_t * cos_theta_t)
            / (eta_i * cos_theta_i + eta_t * cos_theta_t);

        (r_parl.powi(2) + r_perp.powi(2)) * 0.5
    }

    pub fn metal_brdf(&self, ray: Ray, hit: Hit, rng: &mut TlsWyRand) -> Option<Ray> {
        let direction = ray.direction.reflect(hit.normal);
        Some(Ray::new(
            hit.position,
            direction + self.roughness * Vec3::random_in_unit_sphere(rng),
            RayType::Metallic,
        ))
    }

    fn fresnel_conductor(mut cos_theta_i: f64, eta_i: Color, eta_t: Color, k: Color) -> Color {
        fn sqrt_color(color: Color) -> Color {
            Color {
                r: color.r.sqrt(),
                g: color.g.sqrt(),
                b: color.b.sqrt(),
            }
        }

        fn pow2(color: Color) -> Color {
            Color {
                r: color.r.powi(2),
                g: color.g.powi(2),
                b: color.b.powi(2),
            }
        }

        cos_theta_i = cos_theta_i.clamp(-1.0, 1.0);

        let eta = eta_t / eta_i;
        let eta_k = k / eta_i;

        let cos_theta_i2 = cos_theta_i.powi(2);
        let sin_theta_i2 = 1. - cos_theta_i2;

        let eta2 = pow2(eta);
        let eta_k2 = pow2(eta_k);

        let t0 = eta2 - eta_k2 - Color::splat(sin_theta_i2);
        let a2plusb2 = sqrt_color(t0 * t0 + 4.0 * eta2 * eta_k2);
        let t1 = a2plusb2 + Color::splat(cos_theta_i2);
        let a = sqrt_color(0.5 * (a2plusb2 + t0));
        let t2 = 2.0 * cos_theta_i * a;
        let rs = (t1 - t2) / (t1 + t2);

        let t3 = cos_theta_i2 * a2plusb2 + Color::splat(sin_theta_i2 * sin_theta_i2);
        let t4 = t2 * sin_theta_i2;
        let rp = rs * (t3 - t4) / (t3 + t4);

        0.5 * (rp + rs)
    }

    fn diffuse_brdf(&self, hit: Hit, rng: &mut TlsWyRand) -> Option<Ray> {
        let direction = hit.normal + Vec3::random_unit_vector(rng);
        Some(Ray::new(hit.position, direction, RayType::Diffuse))
    }
}

impl Material for PbrMaterial {
    fn next_ray(&self, ray: Ray, hit: Hit, rng: &mut TlsWyRand) -> Option<Ray> {
        let rand = random_float(rng, 0.0, 1.0);
        if rand > self.metallic {
            self.dielectric_bsdf(ray, hit, rng)
        } else {
            self.metal_brdf(ray, hit, rng)
        }
    }

    fn compute_color(
        &self,
        incoming: Ray,
        outgoing: Option<Ray>,
        hit: Hit,
        ray_color: Color,
    ) -> Color {
        let mut result: Color;
        let outgoing = outgoing.unwrap();

        match outgoing.ray_type {
            RayType::Metallic => {
                let cos_theta_i = Vec3::dot(-incoming.direction, hit.normal);
                let fresnel = Self::fresnel_conductor(
                    cos_theta_i,
                    Color::splat(IOR::AIR),
                    self.rgb_ior_n,
                    self.rgb_ior_k,
                );
                result = fresnel * ray_color
            }
            RayType::Dielectric => result = ray_color * self.get_base_color(Point2::from(hit.uv)),
            RayType::Diffuse => {
                result = ray_color
                    * self.get_base_color(Point2::from(hit.uv))
                    * Vec3::dot(outgoing.direction, hit.normal)
            }
        }

        result += self.get_emissive_color(Point2::from(hit.uv));

        result
    }
}
