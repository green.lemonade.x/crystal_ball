use std::default::Default;
use std::f64::consts::{FRAC_PI_2, FRAC_PI_8, PI};
use std::sync::Arc;

use crystal_ball::prelude::*;

fn main() -> Result<(), Error> {
    let box_size = 0.5;
    let cube_size = 0.2;

    let triangle = Triangle::new_with_flat_shading(
        [
            Point3::new(box_size, 0.0, -box_size),
            Point3::new(-box_size, 0.0, box_size),
            Point3::new(box_size, 0.0, box_size),
        ],
        None,
    );
    let plane = Mesh::new(vec![
        triangle,
        triangle.rotate(Point3::default(), Vec3::Y, PI),
    ]);

    let grey_walls = Object::new(
        Arc::new(Mesh::new(vec![
            plane.clone().translate(Vec3::new(0.0, -box_size, 0.0)),
            plane
                .clone()
                .translate(Vec3::new(0.0, -box_size, 0.0))
                .rotate_x(FRAC_PI_2),
            plane
                .clone()
                .translate(Vec3::new(0.0, -box_size, 0.0))
                .rotate_x(PI),
        ])),
        Arc::new(PbrMaterial {
            base_color: Color::WHITE,
            roughness: 1.0,
            ..Default::default()
        }),
    );
    let green_wall = Object::new(
        Arc::new(Mesh::new(vec![plane
            .clone()
            .translate(Vec3::new(0.0, -box_size, 0.0))
            .rotate_z(FRAC_PI_2)])),
        Arc::new(PbrMaterial {
            base_color: Color::new(0.2, 0.4, 0.2),
            roughness: 1.0,
            ..Default::default()
        }),
    );
    let red_wall = Object::new(
        Arc::new(Mesh::new(vec![plane
            .clone()
            .translate(Vec3::new(0.0, -box_size, 0.0))
            .rotate_z(-FRAC_PI_2)])),
        Arc::new(PbrMaterial {
            base_color: Color::new(0.8, 0.2, 0.2),
            roughness: 1.0,
            ..Default::default()
        }),
    );

    let light = Object::new(
        Arc::new(
            plane
                .clone()
                .scale_xyz(Vec3::splat(0.3))
                .translate(Vec3::new(0.0, box_size - 0.001, 0.0)),
        ),
        Arc::new(PbrMaterial {
            base_color: Color::BLACK,
            emissive_color: Color::WHITE,
            emissive_strength: 10.0,
            ..Default::default()
        }),
    );

    let cube = Object::new(
        Arc::new(
            Mesh::new(vec![
                plane
                    .clone()
                    .scale_xyz(Vec3::splat(cube_size))
                    .translate(Vec3::new(0.0, cube_size * box_size, 0.0)),
                plane
                    .clone()
                    .scale_xyz(Vec3::splat(cube_size))
                    .translate(Vec3::new(0.0, -cube_size * box_size, 0.0)),
                plane
                    .clone()
                    .scale_xyz(Vec3::splat(cube_size))
                    .rotate_z(FRAC_PI_2)
                    .translate(Vec3::new(cube_size * box_size, 0.0, 0.0)),
                plane
                    .clone()
                    .scale_xyz(Vec3::splat(cube_size))
                    .rotate_z(-FRAC_PI_2)
                    .translate(Vec3::new(cube_size * box_size, 0.0, 0.0)),
                plane
                    .clone()
                    .scale_xyz(Vec3::splat(cube_size))
                    .rotate_x(FRAC_PI_2)
                    .translate(Vec3::new(0.0, 0.0, cube_size * box_size)),
                plane
                    .clone()
                    .scale_xyz(Vec3::splat(cube_size))
                    .rotate_x(-FRAC_PI_2)
                    .translate(Vec3::new(0.0, 0.0, cube_size * box_size)),
            ])
            .scale_y(2.0)
            .rotate_y(-FRAC_PI_8)
            .translate(Vec3::new(-0.25, -0.3, -0.1)),
        ),
        Arc::new(PbrMaterial {
            base_color: Color::WHITE,
            roughness: 1.0,
            ..Default::default()
        }),
    );

    let spheres = vec![
        Object::new(
            Arc::new(
                Sphere::new()
                    .translate(Vec3::new(0.3, -0.4, 0.15))
                    .scale_xyz(Vec3::splat(0.1)),
            ),
            Arc::new(PbrMaterial {
                base_color: Color::WHITE,
                transmission: 1.0,
                ior: IOR::DIAMOND,
                ..Default::default()
            }),
        ),
        Object::new(
            Arc::new(
                Sphere::new()
                    .translate(Vec3::new(-0.25, -0.05, -0.1))
                    .scale_xyz(Vec3::splat(0.05)),
            ),
            Arc::new(PbrMaterial {
                base_color: Color::WHITE,
                metallic: 1.0,
                ..Default::default()
            }),
        ),
    ];

    let mut objects = vec![grey_walls, green_wall, red_wall, light, cube];

    for sphere in spheres {
        objects.push(sphere);
    }

    let scene = Scene {
        camera: Camera {
            width: 1024,
            height: 1024,
            ..Default::default()
        }
        .translate(Vec3::new(0.0, 0.0, 1.5))
        .look_at(Point3::ZERO, Vec3::Y),
        objects,
        background_color: Color::BLACK,
        ..Default::default()
    };

    let engine = RenderEngine {
        samples: 256,
        max_bounces: 16,
    };
    let image = engine.render(&scene);

    image.write("cornell_box.png")?;

    println!("\nImage saved!");

    Ok(())
}
