//! Geometric shapes that can be placed into the scene.

pub use bvh::BVH;
pub use mesh::Mesh;
pub use object::Object;
pub use sphere::Sphere;
pub use triangle::Triangle;

use crate::math::{Bounds3, Hit, Ray};

mod bvh;
mod mesh;
mod object;
mod sphere;
mod triangle;

pub trait Shape: Send + Sync {
    /// If self and ray intersect, return the corresponding hit.
    /// Otherwise return [`None`](None).
    fn intersects(&self, ray: Ray) -> Option<Hit>;

    fn bounds(&self) -> Bounds3;
}
