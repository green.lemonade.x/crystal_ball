use crate::math::{Bounds3, Hit, Point3, Ray, Transform, Transformable, Vec3};
use crate::shapes::Shape;

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Triangle {
    pub vertices: [Point3; 3],
    pub normals: [Vec3; 3],
    pub uvs: Option<[(f64, f64); 3]>,
}

impl Triangle {
    pub fn new(vertices: [Point3; 3], normals: [Vec3; 3], uvs: Option<[(f64, f64); 3]>) -> Self {
        Triangle {
            vertices,
            normals,
            uvs,
        }
    }

    pub fn new_with_flat_shading(vertices: [Point3; 3], uvs: Option<[(f64, f64); 3]>) -> Self {
        let edge1 = vertices[1] - vertices[0];
        let edge2 = vertices[2] - vertices[0];
        let normal = Vec3::cross(edge1, edge2).normalize();

        Triangle {
            vertices,
            normals: [normal; 3],
            uvs,
        }
    }

    pub fn face_normal(&self) -> Vec3 {
        let edge1 = self.vertices[1] - self.vertices[0];
        let edge2 = self.vertices[2] - self.vertices[0];
        Vec3::cross(edge1, edge2).normalize()
    }

    pub fn circumcenter(&self) -> Point3 {
        let u = self.vertices[1] - self.vertices[0];
        let v = self.vertices[2] - self.vertices[1];
        let w = self.vertices[0] - self.vertices[2];
        let n = Vec3::cross(u, v);

        (self.vertices[0] + self.vertices[1]) * 0.5
            - Vec3::dot(v, w) * Vec3::cross(n, u) / n.magnitude_squared() * 0.5
    }

    pub fn vertex_mean(&self) -> Point3 {
        (self.vertices[0] + self.vertices[1] + self.vertices[2]) / 3.0
    }
}

impl Shape for Triangle {
    fn intersects(&self, ray: Ray) -> Option<Hit> {
        let (edge1, edge2) = (
            self.vertices[1] - self.vertices[0],
            self.vertices[2] - self.vertices[0],
        );

        let p_vec = Vec3::cross(ray.direction, edge2);
        let determinant = Vec3::dot(edge1, p_vec);

        if determinant.abs() < 1e-9 {
            return None;
        }

        let inverse_determinant = 1.0 / determinant;

        let t_vec = ray.origin - self.vertices[0];
        let u = inverse_determinant * Vec3::dot(t_vec, p_vec);

        if !(0.0..=1.0).contains(&u) {
            return None;
        }

        let q_vec = Vec3::cross(t_vec, edge1);
        let v = inverse_determinant * Vec3::dot(ray.direction, q_vec);

        if v < 0.0 || u + v > 1.0 {
            return None;
        }

        let intersection_distance = inverse_determinant * Vec3::dot(edge2, q_vec);

        if intersection_distance < 0.0 {
            return None;
        }

        let intersection_point = ray.get(intersection_distance);

        let w = 1.0 - u - v;
        // Note that the Möller–Trumbore algorithm (https://cadxfem.org/inf/Fast%20MinimumStorage%20RayTriangle%20Intersection.pdf)
        // describes a point T(u,v) by
        // T(u, v) = (1 - u - v) V0 + u V1 + v V2
        // Because u + v + w = 1 and therefore w = 1 - u - v we interpolate by
        // N(u, v, w) = w N0 + u N1 + v N2
        let mut normal =
            (w * self.normals[0] + u * self.normals[1] + v * self.normals[2]).normalize();

        let uv = match self.uvs {
            Some(uvs) => {
                // Same explanation for interpolation as above
                let uv_x = w * uvs[0].0 + u * uvs[1].0 + v * uvs[2].0;
                let uv_y = w * uvs[0].1 + u * uvs[1].1 + v * uvs[2].1;

                (uv_x, uv_y)
            }
            None => (0.0, 0.0),
        };

        if Vec3::dot(ray.direction, normal) > 0.0 {
            normal = -normal
        }

        Some(Hit::new(
            intersection_point,
            normal,
            intersection_distance,
            uv,
        ))
    }

    fn bounds(&self) -> Bounds3 {
        Bounds3::new(self.vertices[0], self.vertices[1]).include_point(self.vertices[2])
    }
}

impl Transformable for Triangle {
    fn translate(mut self, translation: Vec3) -> Self {
        self.vertices[0] += translation;
        self.vertices[1] += translation;
        self.vertices[2] += translation;

        self
    }

    fn rotate(mut self, origin: Point3, axis: Vec3, angle: f64) -> Self {
        let transform = Transform::rotate(origin, axis, angle);

        for vertex in self.vertices.iter_mut() {
            *vertex = transform.mat4 * *vertex;
        }

        for normal in self.normals.iter_mut() {
            *normal = transform.mat4 * *normal;
        }

        self
    }

    fn rotate_x(mut self, angle: f64) -> Self {
        let vertex_mean = self.vertex_mean();
        let transform = Transform::rotate_x(vertex_mean, angle);

        for vertex in self.vertices.iter_mut() {
            *vertex = transform.mat4 * *vertex;
        }

        for normal in self.normals.iter_mut() {
            *normal = transform.mat4 * *normal;
        }

        self
    }

    fn rotate_y(mut self, angle: f64) -> Self {
        let vertex_mean = self.vertex_mean();
        let transform = Transform::rotate_y(vertex_mean, angle);

        for vertex in self.vertices.iter_mut() {
            *vertex = vertex_mean + transform.mat4 * (*vertex - vertex_mean);
        }

        for normal in self.normals.iter_mut() {
            *normal = transform.mat4 * *normal;
        }

        self
    }

    fn rotate_z(mut self, angle: f64) -> Self {
        let vertex_mean = self.vertex_mean();
        let transform = Transform::rotate_z(vertex_mean, angle);

        for vertex in self.vertices.iter_mut() {
            *vertex = vertex_mean + transform.mat4 * (*vertex - vertex_mean);
        }

        for normal in self.normals.iter_mut() {
            *normal = transform.mat4 * *normal;
        }

        self
    }

    fn scale_x(self, factor: f64) -> Self {
        let vertex_mean = self.vertex_mean();

        self.scale(vertex_mean, Vec3::new(factor, 1.0, 1.0))
    }

    fn scale_y(self, factor: f64) -> Self {
        let vertex_mean = self.vertex_mean();

        self.scale(vertex_mean, Vec3::new(1.0, factor, 1.0))
    }

    fn scale_z(self, factor: f64) -> Self {
        let vertex_mean = self.vertex_mean();

        self.scale(vertex_mean, Vec3::new(1.0, 1.0, factor))
    }

    fn scale_xyz(self, scale: Vec3) -> Self {
        let vertex_mean = self.vertex_mean();

        self.scale(vertex_mean, scale)
    }

    fn scale(mut self, origin: Point3, scale: Vec3) -> Self {
        let transform = Transform::scale(origin, scale);

        for vertex in self.vertices.iter_mut() {
            *vertex = transform.mat4 * *vertex;
        }

        for normal in self.normals.iter_mut() {
            *normal = transform.mat4 * *normal;
        }

        self
    }

    // TODO: Implement when I figured out how to preserve the scale
    fn look_at(self, _target: Point3, _view_up: Vec3) -> Self {
        todo!()
    }
}

#[cfg(test)]
mod tests {
    use std::f64::consts::{PI, TAU};

    use assert_approx_eq::assert_approx_eq;

    use crate::math::{Point3, Transformable};
    use crate::util::EPSILON;

    use super::Triangle;

    #[test]
    fn rotate_triangle() {
        let vertices = [
            Point3::new(-1.0, -0.5, -1.0),
            Point3::new(0.0, 1.0, -1.0),
            Point3::new(1.0, -0.5, 2.0),
        ];

        let rotated_x_triangle = Triangle::new_with_flat_shading(vertices, None).rotate_x(TAU);
        let rotated_y_triangle = Triangle::new_with_flat_shading(vertices, None).rotate_y(TAU);
        let rotated_z_triangle = Triangle::new_with_flat_shading(vertices, None).rotate_z(TAU);

        let rotated_triangle = Triangle::new_with_flat_shading(vertices, None)
            .rotate_x(PI)
            .rotate_y(PI)
            .rotate_z(PI);

        assert_approx_eq!(vertices[0], rotated_x_triangle.vertices[0], EPSILON);
        assert_approx_eq!(vertices[1], rotated_x_triangle.vertices[1], EPSILON);
        assert_approx_eq!(vertices[2], rotated_x_triangle.vertices[2], EPSILON);
        assert_approx_eq!(vertices[0], rotated_y_triangle.vertices[0], EPSILON);
        assert_approx_eq!(vertices[1], rotated_y_triangle.vertices[1], EPSILON);
        assert_approx_eq!(vertices[2], rotated_y_triangle.vertices[2], EPSILON);
        assert_approx_eq!(vertices[0], rotated_z_triangle.vertices[0], EPSILON);
        assert_approx_eq!(vertices[1], rotated_z_triangle.vertices[1], EPSILON);
        assert_approx_eq!(vertices[2], rotated_z_triangle.vertices[2], EPSILON);
        assert_approx_eq!(vertices[0], rotated_triangle.vertices[0], EPSILON);
        assert_approx_eq!(vertices[1], rotated_triangle.vertices[1], EPSILON);
        assert_approx_eq!(vertices[2], rotated_triangle.vertices[2], EPSILON);
    }
}
