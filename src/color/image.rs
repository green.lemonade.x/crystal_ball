use std::convert::TryFrom;
use std::fs;
use std::fs::File;
use std::io::BufReader;
use std::path::Path;

use gltf::image::Format;
use image::codecs::hdr::HdrDecoder;
use image::{GenericImageView, ImageBuffer, Rgb};

use crate::color::{Color, Texture};
use crate::util::Error;

/// An image of fixed size represented as a 2-dimensional `Vec` of `Color`.
#[derive(Clone, Debug)]
pub struct Image {
    pub width: usize,
    pub height: usize,
    pub pixels: Vec<Vec<Color>>,
    pub interpolation: Interpolation,
}

// TODO: assert_eq!(width * height, pixels.len())
impl Image {
    /// Create a new image.
    pub fn new(width: usize, height: usize, interpolation: Interpolation) -> Self {
        Image {
            width,
            height,
            pixels: vec![vec![Color::default(); width]; height],
            interpolation,
        }
    }

    /// Create a new image from an already existing 2-dimensional `Vec` of `Colors`.
    pub fn from_pixels(
        width: usize,
        height: usize,
        pixels: Vec<Vec<Color>>,
        interpolation: Interpolation,
    ) -> Self {
        Image {
            width,
            height,
            pixels,
            interpolation,
        }
    }

    pub fn from_raw_with_format(
        width: usize,
        height: usize,
        data: &[u8],
        format: ColorFormat,
        interpolation: Interpolation,
    ) -> Self {
        let pixels = (0..height)
            .map(|y| {
                (0..width)
                    .map(|x| {
                        let offset = match format {
                            ColorFormat::Rgb8 => 3,
                            ColorFormat::Rgba8 => 4,
                            ColorFormat::Rgb16 => 6,
                            ColorFormat::Rgba16 => 8,
                        };

                        let start = offset * (x + y * width);

                        match format {
                            ColorFormat::Rgb8 | ColorFormat::Rgba8 => Color::new(
                                data[start] as f64 / 255.0,
                                data[start + 1] as f64 / 255.0,
                                data[start + 2] as f64 / 255.0,
                            ),
                            ColorFormat::Rgb16 | ColorFormat::Rgba16 => Color::new(
                                (((data[start + 1] as u16) << 8) | data[start] as u16) as f64
                                    / 65535.0,
                                (((data[start + 3] as u16) << 8) | data[start + 2] as u16) as f64
                                    / 65535.0,
                                (((data[start + 5] as u16) << 8) | data[start + 4] as u16) as f64
                                    / 65535.0,
                            ),
                        }
                    })
                    .collect::<Vec<Color>>()
            })
            .collect::<Vec<Vec<Color>>>();

        Image {
            width,
            height,
            pixels,
            interpolation,
        }
    }

    pub fn from_raw(
        width: usize,
        height: usize,
        data: &[u8],
        interpolation: Interpolation,
    ) -> Self {
        Self::from_raw_with_format(width, height, data, ColorFormat::Rgb8, interpolation)
    }

    /// Load an image from the file at the provided path.
    /// This *can* be used to load HDRIs, however good results are most likely using [from_hdr_file](Self::from_hdr_file).
    pub fn from_file<P: AsRef<Path>>(path: P, interpolation: Interpolation) -> Result<Self, Error> {
        let image = image::open(path)?;
        let mut pixels =
            vec![vec![Color::default(); image.width() as usize]; image.height() as usize];
        pixels.iter_mut().enumerate().for_each(|(y, row)| {
            row.iter_mut().enumerate().for_each(|(x, color)| {
                let pixel = image.get_pixel(x as u32, y as u32);
                *color = Color::new(
                    pixel.0[0] as f64 / 255.0,
                    pixel.0[1] as f64 / 255.0,
                    pixel.0[2] as f64 / 255.0,
                );
            });
        });

        Ok(Image {
            width: image.width() as usize,
            height: image.height() as usize,
            pixels,
            interpolation,
        })
    }

    /// Load an HDRI from the file at the provided path.
    /// Conversion into RGB space is according to the formula
    /// v' = (v * scale)<sup>gamma</sup>
    pub fn from_hdr_file<P: AsRef<Path>>(
        path: P,
        interpolation: Interpolation,
    ) -> Result<Self, Error> {
        let decoder = HdrDecoder::new(BufReader::new(File::open(path)?))?;

        let width = decoder.metadata().width as usize;
        let height = decoder.metadata().height as usize;

        let image = decoder.read_image_hdr()?;

        let mut pixels = vec![vec![Color::default(); width]; height];
        pixels.iter_mut().enumerate().for_each(|(y, row)| {
            row.iter_mut().enumerate().for_each(|(x, color)| {
                let pixel = image[x + y * width];

                *color = Color::new(pixel.0[0] as f64, pixel.0[1] as f64, pixel.0[2] as f64);
            });
        });

        Ok(Image {
            width,
            height,
            pixels,
            interpolation,
        })
    }

    /// Create an image from a `Vec` of `ImageTiles`.
    pub(crate) fn from_tiles(
        width: usize,
        height: usize,
        tiles: &[ImageTile],
        interpolation: Interpolation,
    ) -> Self {
        let mut pixels = vec![vec![Color::default(); width]; height];

        for tile in tiles {
            for (i, row) in tile.image().pixels.iter().enumerate() {
                pixels[(tile.y() * tile.image().height + i).min(height - 1)].splice(
                    tile.x() * tile.image().width
                        ..((tile.x() + 1) * tile.image().width).min(width - 1),
                    row.as_slice().iter().cloned(),
                );
            }
        }

        Image {
            width,
            height,
            pixels,
            interpolation,
        }
    }

    /// Return the dimensions of the image
    pub fn dimensions(&self) -> (usize, usize) {
        (self.width, self.height)
    }

    /// Return the color value of the pixel `(x, y)`.
    /// The image is considered to wrap periodically and is thus virtually infinite.
    pub fn get_pixel(&self, x: f64, y: f64) -> Color {
        match self.interpolation {
            Interpolation::Closest => self.interpolate_closest(x, y),
            Interpolation::Bilinear => self.interpolate_bilinear(x, y),
        }
    }

    /// Return the closest color value at the position `(x, y)`
    fn interpolate_closest(&self, x: f64, y: f64) -> Color {
        self.get_pixel_periodic(x.round(), y.round())
    }

    /// Return the bilinearly interpolated color value at the position `(x, y)`.
    fn interpolate_bilinear(&self, x: f64, y: f64) -> Color {
        let x1 = x.floor();
        let x2 = x.ceil();
        let y1 = y.floor();
        let y2 = y.ceil();

        let (weight1, weight2) = if x1 == x2 {
            (1.0, 0.0)
        } else {
            ((x2 - x) / (x2 - x1), (x - x1) / (x2 - x1))
        };

        let color1 =
            self.get_pixel_periodic(x1, y1) * weight1 + self.get_pixel_periodic(x2, y1) * weight2;
        let color2 =
            self.get_pixel_periodic(x1, y2) * weight1 + self.get_pixel_periodic(x2, y2) * weight2;

        if y1 == y2 {
            color1
        } else {
            (color1 * (y2 - y) + color2 * (y - y1)) / (y2 - y1)
        }
    }

    fn get_pixel_periodic(&self, x: f64, y: f64) -> Color {
        self.pixels[y.rem_euclid(self.height as f64) as usize]
            [x.rem_euclid(self.width as f64) as usize]
    }

    /// Set the color value of the pixel `(x, y)`.
    /// Return an error if the pixel is out of bounds.
    pub fn set_pixel(&mut self, x: usize, y: usize, color: Color) -> Result<(), Error> {
        if y + 1 > self.height || x + 1 > self.width {
            return Err(format!(
                "Coordinates ({}, {}) are out of image bounds ({}, {})",
                x, y, self.width, self.height
            ))?;
        }

        self.pixels[y][x] = color;
        Ok(())
    }

    /// Write the image to a file at the provided path.
    pub fn write<P: AsRef<Path>>(&self, path: P) -> Result<(), Error> {
        let mut image_buffer = ImageBuffer::new(self.width as u32, self.height as u32);
        image_buffer
            .enumerate_pixels_mut()
            .for_each(|(x, y, pixel)| {
                let color = self.pixels[y as usize][x as usize];
                *pixel = Rgb([
                    (color.r * 255.0).round().min(255.0).max(0.0) as u8,
                    (color.g * 255.0).round().min(255.0).max(0.0) as u8,
                    (color.b * 255.0).round().min(255.0).max(0.0) as u8,
                ])
            });

        image_buffer.save(&path)?;

        println!("Saved image to {:?}", fs::canonicalize(path)?);

        Ok(())
    }

    /// Denoise the image using the OIDN library.
    #[cfg_attr(doc, doc(cfg(feature = "oidn")))]
    #[cfg(feature = "oidn")]
    pub fn denoise(&mut self) -> Result<(), Error> {
        use oidn::{Device, RayTracing};
        use std::time::Instant;

        let width = self.width;
        let height = self.height;

        let mut input_img = vec![0.0f32; 3 * width * height];

        for y in 0..height {
            for x in 0..width {
                let pixel = self.pixels()[y][x];
                for c in 0..3 {
                    input_img[3 * (y * width + x) + c] = match c {
                        0 => pixel.r as f32,
                        1 => pixel.g as f32,
                        2 => pixel.b as f32,
                        _ => 0.0,
                    };
                }
            }
        }

        let mut filter_output = vec![0.0f32; input_img.len()];

        println!("Denoising...");

        let start_time = Instant::now();

        let device = Device::new();
        let mut filter = RayTracing::new(&device);
        filter.srgb(true).image_dimensions(width, height);
        filter.filter(&input_img[..], &mut filter_output[..])?;

        if let Err((e, _)) = device.get_error() {
            return Err(e)?;
        }

        println!(
            "Denoising finished after {:.2?}",
            Instant::now() - start_time
        );

        for i in (0..filter_output.len()).step_by(3) {
            let pixel = i / 3;

            let x = pixel % width;
            let y = pixel / width;

            let color = Color::new(
                filter_output[i] as f64,
                filter_output[i + 1] as f64,
                filter_output[i + 2] as f64,
            );

            // TODO: Just use an iterator and collect values
            self.set_pixel(x, y, color).unwrap();
        }

        Ok(())
    }
}

impl Texture for Image {
    fn get_pixel(&self, u: f64, v: f64) -> Color {
        self.get_pixel((self.width - 1) as f64 * u, (self.height - 1) as f64 * v)
    }
}

/// A tile of an image storing its position and its chunk of the original image.
#[derive(Clone, Debug)]
pub(crate) struct ImageTile {
    x: usize,
    y: usize,
    image: Image,
}

impl ImageTile {
    /// Create a new ImageTile.
    pub fn new(x: usize, y: usize, image: Image) -> Self {
        ImageTile { x, y, image }
    }

    /// Return the position of the image tile.
    pub fn position(&self) -> (usize, usize) {
        (self.x, self.y)
    }

    /// Return the x position of the image tile.
    pub fn x(&self) -> usize {
        self.x
    }

    /// Return the y position of the image tile.
    pub fn y(&self) -> usize {
        self.y
    }

    /// Return a reference of the part of the original image.
    pub fn image(&self) -> &Image {
        &self.image
    }
    /// Return a mutable reference of the part of the original image.
    pub fn image_mut(&mut self) -> &mut Image {
        &mut self.image
    }
}

#[derive(Copy, Clone, Debug)]
pub enum Interpolation {
    Closest,
    Bilinear,
}

#[derive(Copy, Clone, Debug)]
pub enum ColorFormat {
    Rgb8,
    Rgba8,
    Rgb16,
    Rgba16,
}

impl TryFrom<Format> for ColorFormat {
    type Error = Error;

    fn try_from(format: Format) -> Result<Self, Self::Error> {
        match format {
            Format::R8G8B8 => Ok(ColorFormat::Rgb8),
            Format::R8G8B8A8 => Ok(ColorFormat::Rgba8),
            Format::R16G16B16 => Ok(ColorFormat::Rgb16),
            Format::R16G16B16A16 => Ok(ColorFormat::Rgba16),
            _ => Err(Error::UnsupportedColorFormat(format)),
        }
    }
}
