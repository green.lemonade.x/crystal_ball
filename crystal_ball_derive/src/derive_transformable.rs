use quote::quote;
use syn::{parse_macro_input, Data, DeriveInput, Field, Fields};

pub fn derive_transformable(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input = parse_macro_input!(input as DeriveInput);
    let name = input.ident;

    let crate_name = if !input.attrs.is_empty() {
        quote!(crate)
    } else {
        quote!(crystal_ball)
    };

    let Data::Struct(data) = input.data else {
        return quote!(compile_error!("Expected struct")).into();
    };

    let Fields::Named(fields) = data.fields else {
        return quote!(compile_error!("Expected named fields.")).into();
    };

    let transform_fields = fields
        .named
        .iter()
        .filter(|field| field.attrs.len() == 1)
        .collect::<Vec<&Field>>();

    if transform_fields.len() != 1 {
        return quote!(compile_error!(
            "Expected exactly 1 `#[transform]` attribute."
        ))
        .into();
    }

    let transform = transform_fields[0].ident.clone().unwrap();

    quote!(
        impl #crate_name::math::Transformable for #name {
            fn translate(mut self, translation: #crate_name::math::Vec3) -> Self {
                let transform = #crate_name::math::Transform::translate(translation);
                self.#transform = #crate_name::math::Transform::new(
                    transform.mat4 * self.#transform.mat4,
                    self.#transform.mat4_inverse * transform.mat4_inverse,
                );

                self
            }

            fn rotate(mut self, origin: #crate_name::math::Point3, axis: #crate_name::math::Vec3, angle: f64) -> Self {
                let transform = #crate_name::math::Transform::rotate(origin, axis, angle);
                self.#transform = #crate_name::math::Transform::new(
                    transform.mat4 * self.#transform.mat4,
                    self.#transform.mat4_inverse * transform.mat4_inverse,
                );

                self
            }

            fn rotate_x(mut self, angle: f64) -> Self {
                let mat4 = self.#transform.mat4;
                let origin = #crate_name::math::Point3::new(mat4[0][3], mat4[1][3], mat4[2][3]);

                let transform = #crate_name::math::Transform::rotate_x(origin, angle);
                self.#transform = #crate_name::math::Transform::new(
                    transform.mat4 * self.#transform.mat4,
                    self.#transform.mat4_inverse * transform.mat4_inverse,
                );

                self
            }

            fn rotate_y(mut self, angle: f64) -> Self {
                let mat4 = self.#transform.mat4;
                let origin = #crate_name::math::Point3::new(mat4[0][3], mat4[1][3], mat4[2][3]);

                let transform = #crate_name::math::Transform::rotate_y(origin, angle);
                self.#transform = #crate_name::math::Transform::new(
                    transform.mat4 * self.#transform.mat4,
                    self.#transform.mat4_inverse * transform.mat4_inverse,
                );

                self
            }

            fn rotate_z(mut self, angle: f64) -> Self {
                let mat4 = self.#transform.mat4;
                let origin = #crate_name::math::Point3::new(mat4[0][3], mat4[1][3], mat4[2][3]);

                let transform = #crate_name::math::Transform::rotate_z(origin, angle);
                self.#transform = #crate_name::math::Transform::new(
                    transform.mat4 * self.#transform.mat4,
                    self.#transform.mat4_inverse * transform.mat4_inverse,
                );

                self
            }

            fn scale_x(self, factor: f64) -> Self {
                let mat4 = self.#transform.mat4;
                let origin = #crate_name::math::Point3::new(mat4[0][3], mat4[1][3], mat4[2][3]);

                self.scale(origin, #crate_name::math::Vec3::new(factor, 1.0, 1.0))
            }

            fn scale_y(self, factor: f64) -> Self {
                let mat4 = self.#transform.mat4;
                let origin = #crate_name::math::Point3::new(mat4[0][3], mat4[1][3], mat4[2][3]);

                self.scale(origin, #crate_name::math::Vec3::new(1.0, factor, 1.0))
            }

            fn scale_z(self, factor: f64) -> Self {
                let mat4 = self.#transform.mat4;
                let origin = #crate_name::math::Point3::new(mat4[0][3], mat4[1][3], mat4[2][3]);

                self.scale(origin, #crate_name::math::Vec3::new(1.0, 1.0, factor))
            }

            fn scale_xyz(self, scale: #crate_name::math::Vec3) -> Self {
                let mat4 = self.#transform.mat4;
                let origin = #crate_name::math::Point3::new(mat4[0][3], mat4[1][3], mat4[2][3]);

                self.scale(origin, scale)
            }

            fn scale(mut self, origin: #crate_name::math::Point3, scale: #crate_name::math::Vec3) -> Self {
                let transform = #crate_name::math::Transform::scale(origin, scale);
                self.#transform = #crate_name::math::Transform::new(
                    transform.mat4 * self.#transform.mat4,
                    self.#transform.mat4_inverse * transform.mat4_inverse,
                );

                self
            }

            // Assumes that view_up is normalized
            // Will discard scale
            fn look_at(mut self, target: #crate_name::math::Point3, view_up: #crate_name::math::Vec3) -> Self {
                let mat4 = self.#transform.mat4;
                let origin = #crate_name::math::Point3::new(mat4[0][3], mat4[1][3], mat4[2][3]);

                self.#transform = #crate_name::math::Transform::look_at(origin, target, view_up);

                self
            }
        }
    )
    .into()
}
