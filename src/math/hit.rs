use crate::math::{Point3, Vec3};

/// The intersection of a ray with an object.
#[derive(Copy, Clone, Debug)]
pub struct Hit {
    pub position: Point3,
    pub normal: Vec3,
    pub distance: f64,
    pub uv: (f64, f64),
}

impl Hit {
    /// Create a new hit.
    pub fn new(position: Point3, normal: Vec3, distance: f64, uv: (f64, f64)) -> Self {
        Hit {
            position,
            normal,
            distance,
            uv,
        }
    }
}
