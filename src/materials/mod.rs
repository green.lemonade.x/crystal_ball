//! Data Types and functions related to materials and UV-Mapping.
use nanorand::tls::TlsWyRand;

pub use pbr_material::PbrMaterial;

use crate::color::Color;
use crate::math::{Hit, Ray};

mod pbr_material;
/// Characterize optical properties of a surface.
pub trait Material: Send + Sync {
    /// Generate the next ray to be traced.
    fn next_ray(&self, ray: Ray, hit: Hit, rng: &mut TlsWyRand) -> Option<Ray>;

    /// Return the color at the given coordinates.
    fn compute_color(
        &self,
        incoming: Ray,
        outgoing: Option<Ray>,
        hit: Hit,
        ray_color: Color,
    ) -> Color;
}
