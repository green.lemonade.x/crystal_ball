use std::sync::Arc;

use crystal_ball::prelude::*;

fn main() -> Result<(), Error> {
    // let objects =
    //     Object::load_gltf("glTF_sample_models/2.0/ABeautifulGame/glTF/ABeautifulGame.gltf")?;
    let objects = Object::load_gltf("../../BlenderProjects/Challenges/Impact/SledgeHammer.glb")?;
    // let objects = vec![Object::new(
    //     Sphere::new().translate(Vec3::new(0.0, 0.0, -5.0)),
    //     Emissive::new(Color::new(1.0, 0.0, 0.0), 1.0),
    // )];

    let scene = Scene {
        camera: Camera {
            width: 1280,
            height: 720,
            // TODO: FOV is wrong by almost a factor of 2?
            fov: 14f64.to_radians(),
            ..Default::default()
        }
        .translate(Vec3::new(-0.83448, 2.8324, 3.9522))
        .rotate_y(-6.8988f64.to_radians()),
        // .translate(Vec3::new(7.0, 1.0, 0.0)),
        // .translate(Vec3::new(1.0, 0.5, 0.5))
        // .translate(Vec3::new(0.84, 1.46, -1.81))
        // .rotate_x(-33_f64.to_radians())
        // .rotate_y(180_f64.to_radians()),
        objects,
        // background: Arc::new(Color::splat(0.0)),
        background_color: Color::WHITE,
        background_texture: Some(Arc::new(
            Image::from_hdr_file(
                "../../BlenderProjects/Challenges/Impact/peppermint_powerplant_4k.hdr",
                Interpolation::Closest,
            )
            .unwrap(),
            // Image::from_hdr_file(
            //     "../../BlenderProjects/Challenges/Impact/peppermint_powerplant_4k.hdr",
            //     Interpolation::Closest,
            // )
            // .unwrap(),
        )),
        background_transform: Transform::default()
            .rotate_x(15f64.to_radians())
            .rotate_y(254f64.to_radians())
            .rotate_z(-2f64.to_radians()),
        ..Default::default()
    };

    let engine = RenderEngine {
        samples: 64,
        max_bounces: 16,
    };
    let mut image = engine.render(&scene);

    image.write("output/image.png")?;

    #[cfg(feature = "oidn")]
    {
        image.denoise()?;
        image.write("output/image_denoised.png")?;
    }

    println!("\nImage saved!");

    Ok(())
}
