use std::f64::consts::FRAC_1_PI;

use crystal_ball_derive::Transformable;

use crate::math::{Bounds3, Hit, Point3, Ray, Transform, Vec3};
use crate::shapes::Shape;

/// A geometrically perfect sphere.
#[derive(Copy, Clone, Default, Debug, PartialEq, Transformable)]
#[internal]
pub struct Sphere {
    #[transform]
    pub transform: Transform,
}

impl Sphere {
    /// Create a new sphere.
    pub fn new() -> Self {
        Sphere {
            transform: Transform::default(),
        }
    }

    pub fn uv_map(&self, point: Point3) -> (f64, f64) {
        let u = 0.5 + point.z.atan2(point.x) * 0.5 * FRAC_1_PI;
        let v = 0.5 - point.y.asin() * FRAC_1_PI;

        (u, v)
    }
}

impl Shape for Sphere {
    fn intersects(&self, ray: Ray) -> Option<Hit> {
        let ray = self.transform.inverse_transform_ray(ray);

        // If the ray origin is outside of the sphere,
        // tangent_squared is the square of the vector from the ray origin to a tangent point on the sphere.
        // If it is inside, tangent_squared will be < 0, but the formulas below still hold.
        let tangent_squared = ray.origin.to_vec3().magnitude_squared() - 1.0;
        let projection = Vec3::dot(ray.direction, ray.origin.into());
        let discriminant = projection.powi(2) - tangent_squared;

        if discriminant <= 0.0 {
            return None;
        }

        let mut intersection_distance = if tangent_squared > 0.0 {
            // Ray origin is outside the sphere.
            -projection - discriminant.sqrt()
        } else {
            // Ray origin is inside the sphere.
            -projection + discriminant.sqrt()
        };

        if intersection_distance < 0.0 {
            return None;
        }

        let mut intersection_point = ray.get(intersection_distance);

        let mut normal = intersection_point.to_vec3();
        let (u, v) = self.uv_map((-normal).into());

        let ray = self.transform.transform_ray(ray);

        intersection_point = self.transform.mat4 * intersection_point;
        intersection_distance = (intersection_point - ray.origin).magnitude();

        normal = (self.transform.mat4_inverse.transpose() * normal).normalize();

        if Vec3::dot(ray.direction, normal) > 0.0 {
            normal = -normal
        }

        Some(Hit::new(
            intersection_point,
            normal,
            intersection_distance,
            (1.0 - u, 1.0 - v),
        ))
    }

    fn bounds(&self) -> Bounds3 {
        let min = Point3::splat(-1.0);
        let max = Point3::splat(1.0);

        self.transform.transform_bounds(Bounds3::new(min, max))
    }
}
