use std::collections::{BTreeSet, HashMap};
use std::convert::TryFrom;
use std::path::Path;
use std::sync::Arc;

use gltf::{Buffer, Document, Node};
use gltf::image::Data;
use gltf::mesh::Reader;
use gltf::texture::MagFilter;

use crate::color::{Color, ColorFormat, Image, Interpolation, Texture};
use crate::materials::{Material, PbrMaterial};
use crate::math::{Bounds3, Hit, Mat4, Point3, Ray, Vec3};
use crate::shapes::{Mesh, Shape, Triangle};
use crate::util::Error;

#[derive(Clone)]
pub struct Object {
    // TODO: Arc ist pointless, cause you need to create a new shape if you want to change it's transform
    pub shape: Arc<dyn Shape>,
    pub material: Arc<dyn Material>,
}

impl Object {
    pub fn new(shape: Arc<dyn Shape>, material: Arc<dyn Material>) -> Self {
        Object { shape, material }
    }

    fn apply_transform_recursively(transform_matrices: &mut [Mat4], node: &Node) {
        node.children().for_each(|c| {
            transform_matrices[c.index()] =
                transform_matrices[node.index()] * transform_matrices[c.index()];

            Self::apply_transform_recursively(transform_matrices, &c);
        });
    }

    fn load_vertices<'a, 's, F: Clone + Fn(Buffer<'a>) -> Option<&'s [u8]>>(
        reader: &Reader<'a, 's, F>,
        matrix: Mat4,
        node: &Node,
    ) -> Option<Vec<Point3>> {
        let vertices_vec = match reader.read_positions() {
            Some(vertices) => vertices
                .map(|v| matrix * Point3::new(v[0] as f64, v[1] as f64, v[2] as f64))
                .collect::<Vec<Point3>>(),
            None => {
                eprint!("ERROR: Attribute POSITION not found. ");
                match node.name() {
                    Some(name) => eprintln!("Skipping '{}'", name),
                    None => eprintln!("Skipping mesh"),
                }

                return None;
            }
        };

        Some(vertices_vec)
    }

    fn load_normals<'a, 's, F: Clone + Fn(Buffer<'a>) -> Option<&'s [u8]>>(
        reader: &Reader<'a, 's, F>,
        matrix: Mat4,
        node: &Node,
    ) -> Option<Vec<Vec3>> {
        match reader.read_normals() {
            Some(normals) => Some(
                normals
                    .map(|n| matrix * Vec3::new(n[0] as f64, n[1] as f64, n[2] as f64))
                    .collect::<Vec<Vec3>>(),
            ),
            None => {
                eprint!("WARNING: Attribute NORMAL not found. ");
                match node.name() {
                    Some(name) => eprintln!("Generating normals for '{}'", name),
                    None => eprintln!("Generating normals"),
                }
                None
            }
        }
    }

    fn load_uvs<'a, 's, F: Clone + Fn(Buffer<'a>) -> Option<&'s [u8]>>(
        reader: &Reader<'a, 's, F>,
        node: &Node,
    ) -> Option<Vec<(f64, f64)>> {
        match reader.read_tex_coords(0) {
            Some(tex_coords) => Some(
                tex_coords
                    .into_f32()
                    .map(|t| (t[0] as f64, t[1] as f64))
                    .collect::<Vec<(f64, f64)>>(),
            ),
            None => {
                eprint!("WARNING: Attribute TEXCOORD not found. ");
                match node.name() {
                    Some(name) => {
                        eprintln!("'{}' will only support colors, not textures", name)
                    }
                    None => eprintln!("Mesh will only support colors, not textures"),
                }
                None
            }
        }
    }

    fn generate_triangles<'a, 's, F: Clone + Fn(Buffer<'a>) -> Option<&'s [u8]>>(
        reader: &Reader<'a, 's, F>,
        vertices_vec: Vec<Point3>,
        normals_vec: Option<Vec<Vec3>>,
        uvs_vec: Option<Vec<(f64, f64)>>,
        node: &Node,
    ) -> Option<Vec<Triangle>> {
        let triangles = match reader.read_indices() {
            Some(indices) => {
                let indices_vec = indices.into_u32().collect::<Vec<u32>>();
                indices_vec
                    .chunks(3)
                    .map(|i| {
                        let vertices = [
                            vertices_vec[i[0] as usize],
                            vertices_vec[i[1] as usize],
                            vertices_vec[i[2] as usize],
                        ];
                        let uvs = uvs_vec.as_ref().map(|uvs| {
                            [uvs[i[0] as usize], uvs[i[1] as usize], uvs[i[2] as usize]]
                        });

                        match &normals_vec {
                            Some(normals) => Triangle::new(
                                vertices,
                                [
                                    normals[i[0] as usize],
                                    normals[i[1] as usize],
                                    normals[i[2] as usize],
                                ],
                                uvs,
                            ),
                            None => Triangle::new_with_flat_shading(vertices, uvs),
                        }
                    })
                    .collect::<Vec<Triangle>>()
            }
            None => {
                eprint!("ERROR: Indices not found. ");
                match node.name() {
                    Some(name) => eprintln!("Skipping '{}'", name),
                    None => eprintln!("Skipping mesh"),
                }

                return None;
            }
        };

        Some(triangles)
    }

    fn load_images(
        document: &Document,
        images: &[Data],
    ) -> HashMap<usize, Option<Arc<dyn Texture>>> {
        document
            .textures()
            .map(|t| {
                let index = t.source().index();
                let data = &images[index];

                (index, Self::load_image(data, &t))
            })
            .collect()
    }

    // TODO: Cache using index -> Arc::clone()
    fn load_image(data: &Data, texture: &gltf::Texture) -> Option<Arc<dyn Texture>> {
        let format = match ColorFormat::try_from(data.format) {
            Ok(f) => f,
            Err(e) => {
                println!("{}", e);

                return None;
            }
        };

        let image = Image::from_raw_with_format(
            data.width as usize,
            data.height as usize,
            &data.pixels,
            format,
            match texture.sampler().mag_filter() {
                None => Interpolation::Bilinear,
                Some(filter) => match filter {
                    MagFilter::Nearest => Interpolation::Closest,
                    MagFilter::Linear => Interpolation::Bilinear,
                },
            },
        );

        Some(Arc::new(image))
    }

    fn load_materials(
        document: &Document,
        images: &HashMap<usize, Option<Arc<dyn Texture>>>,
    ) -> HashMap<usize, Arc<PbrMaterial>> {
        document
            .materials()
            .filter(|m| m.index().is_some())
            .map(|m| (m.index().unwrap(), Arc::new(Self::load_material(images, m))))
            .collect()
    }

    fn load_material(
        images: &HashMap<usize, Option<Arc<dyn Texture>>>,
        material: gltf::Material,
    ) -> PbrMaterial {
        let pbr = material.pbr_metallic_roughness();

        let base_color_factor = pbr.base_color_factor().map(f64::from);
        let base_color = Color::try_from(&base_color_factor[0..3]).unwrap();
        let base_color_texture = pbr
            .base_color_texture()
            .and_then(|i| images[&i.texture().index()].as_ref().map(Arc::clone));

        let metallic = pbr.metallic_factor() as f64;
        let roughness = pbr.roughness_factor() as f64;

        let transmission = material
            .transmission()
            .map(|t| t.transmission_factor())
            .unwrap_or_default() as f64;
        let ior = material.ior().unwrap_or(1.0) as f64;

        let emissive_color_factor = material.emissive_factor().map(f64::from);
        let emissive_color = Color::from(emissive_color_factor);
        let emissive_texture = material
            .emissive_texture()
            .and_then(|i| images[&i.texture().index()].as_ref().map(Arc::clone));
        let emissive_strength = material.emissive_strength().unwrap_or_default() as f64;

        PbrMaterial {
            base_color,
            base_color_texture,
            metallic,
            roughness,
            transmission,
            ior,
            emissive_color,
            emissive_texture,
            emissive_strength,
            ..Default::default()
        }
    }

    // TODO: Document exactly what works and what doesn't
    pub fn load_gltf<P: AsRef<Path>>(path: P) -> Result<Vec<Object>, Error> {
        let (document, buffers, images) = gltf::import(path)?;

        let mut nodes = document.nodes().collect::<Vec<Node>>();
        nodes.sort_by_key(|n| n.index());

        let nodes_indices = nodes.iter().map(|n| n.index()).collect::<BTreeSet<usize>>();

        let children_indices = nodes
            .iter()
            .flat_map(|n| n.children().map(|c| c.index()))
            .collect::<BTreeSet<usize>>();

        let mut transform_matrices = nodes
            .iter()
            .map(|n| Mat4::from(n.transform().matrix().map(|r| r.map(f64::from))).transpose())
            .collect::<Vec<Mat4>>();

        nodes_indices.difference(&children_indices).for_each(|i| {
            Self::apply_transform_recursively(&mut transform_matrices, &nodes[*i]);
        });

        let mut objects = vec![];

        let images = Self::load_images(&document, &images);
        let materials = Self::load_materials(&document, &images);

        for node in nodes {
            if let Some(mesh) = node.mesh() {
                for primitive in mesh.primitives() {
                    let reader = primitive.reader(|buffer| Some(&buffers[buffer.index()]));

                    let matrix = transform_matrices[node.index()];

                    let vertices_vec = match Self::load_vertices(&reader, matrix, &node) {
                        Some(vertices) => vertices,
                        None => continue,
                    };

                    let normals_vec = Self::load_normals(&reader, matrix, &node);

                    let uvs_vec = Self::load_uvs(&reader, &node);

                    let triangles = match Self::generate_triangles(
                        &reader,
                        vertices_vec,
                        normals_vec,
                        uvs_vec,
                        &node,
                    ) {
                        Some(triangles) => triangles,
                        None => continue,
                    };

                    let material = match primitive.material().index() {
                        None => Arc::new(PbrMaterial::default()),
                        Some(i) => Arc::clone(&materials[&i]),
                    };

                    objects.push(Object::new(Arc::new(Mesh::new(triangles)), material));
                }
            }
        }

        Ok(objects)
    }
}

impl Shape for Object {
    fn intersects(&self, ray: Ray) -> Option<Hit> {
        self.shape.intersects(ray)
    }

    fn bounds(&self) -> Bounds3 {
        self.shape.bounds()
    }
}
