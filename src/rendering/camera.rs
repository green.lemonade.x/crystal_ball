use std::f64::consts::FRAC_PI_4;

use nanorand::tls::TlsWyRand;

use crystal_ball_derive::Transformable;

use crate::math::{Bounds2, Point2, Point3, Ray, RayType, Transform, Vec2, Vec3};
use crate::util::random_float;

#[derive(Copy, Clone, Debug)]
pub struct PrecalculatedCamera {
    pub camera_to_world: Transform,
    pub camera_to_screen: Transform,
    pub raster_to_camera: Transform,
    pub screen_to_raster: Transform,
    pub lens_radius: f64,
    pub focal_distance: f64,
    pub width: u32,
    pub height: u32,
}

impl From<Camera> for PrecalculatedCamera {
    fn from(c: Camera) -> Self {
        let lens_radius = c.lens_radius;
        let focal_distance = c.focal_distance;

        let aspect = c.width as f64 / c.height as f64;

        let screen_window = if aspect > 1.0 {
            Bounds2::new(Point2::new(-aspect, -1.0), Point2::new(aspect, 1.0))
        } else {
            Bounds2::new(
                Point2::new(-1.0, -1.0 / aspect),
                Point2::new(1.0, 1.0 / aspect),
            )
        };

        let camera_to_screen = Transform::perspective(c.fov, 1e-2, 1e3);
        let screen_to_raster =
            Transform::scale(
                Point3::ZERO,
                Vec3::new(c.width as f64, c.height as f64, 1.0),
            ) * Transform::scale(
                Point3::ZERO,
                Vec3::new(
                    1.0 / (screen_window.min.x - screen_window.max.x),
                    1.0 / (screen_window.max.y - screen_window.min.y),
                    1.0,
                ),
            ) * Transform::translate(Vec3::new(-screen_window.max.x, -screen_window.min.y, 0.0));

        let raster_to_screen = screen_to_raster.inverse();

        let raster_to_camera = camera_to_screen.inverse() * raster_to_screen;

        Self {
            camera_to_world: c.transform,
            camera_to_screen,
            raster_to_camera,
            screen_to_raster,
            lens_radius,
            focal_distance,
            width: c.width,
            height: c.height,
        }
    }
}

impl PrecalculatedCamera {
    pub fn get_ray(&self, sample: CameraSample) -> Ray {
        let p_film = Point3::new(sample.pos_film.x, sample.pos_film.y, 0.0);
        let p_camera = self.raster_to_camera.mat4 * p_film;
        let mut ray = Ray::new(
            Point3::ZERO,
            Vec3::from(p_camera).normalize(),
            RayType::Dielectric,
        );

        if self.lens_radius > 0.0 {
            let p_lens = self.lens_radius * sample.pos_lens.sample_concentric_on_disc();

            let ft = self.focal_distance / ray.direction.z;
            let focus_point = ray.get(ft);

            ray.origin = Point3::new(p_lens.x, p_lens.y, 0.0);
            ray.direction = (focus_point - ray.origin).normalize()
        }
        self.camera_to_world.transform_ray(ray)
    }

    pub fn dimensions(&self) -> (u32, u32) {
        (self.width, self.height)
    }
}

#[derive(Copy, Clone, Debug, Transformable)]
#[internal]
pub struct Camera {
    #[transform]
    pub transform: Transform,
    pub width: u32,
    pub height: u32,
    pub fov: f64,
    pub focal_distance: f64,
    pub lens_radius: f64,
}

impl Default for Camera {
    fn default() -> Self {
        Self {
            transform: Transform::default(),
            width: 1920,
            height: 1080,
            fov: FRAC_PI_4,
            focal_distance: 10.0,
            lens_radius: 0.0,
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub struct CameraSample {
    pub pos_film: Vec2,
    pub pos_lens: Vec2,
}

impl CameraSample {
    pub fn new(x: f64, y: f64, rng: &mut TlsWyRand, height: f64) -> Self {
        let u = x + random_float(rng, 0.0, 1.0);
        let v = height - y + random_float(rng, 0.0, 1.0);

        let pos_film = Vec2::new(u, v);
        let pos_lens = Vec2::random(rng, 0.0, 1.0);

        CameraSample { pos_film, pos_lens }
    }
}
